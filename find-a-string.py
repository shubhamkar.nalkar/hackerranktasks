# https://www.hackerrank.com/challenges/find-a-string/problem?isFullScreen=true


def count_substring(string, sub_string):
    lenSub = len(sub_string)
    lenStr = len(string)
    val = 0

    end = lenStr - lenSub
    for i in range(0, end + 1):
        short = string[i : lenSub + i]
        if short == sub_string:
            val = val + 1

    return val


if __name__ == "__main__":
    # string = input().strip()
    # sub_string = input().strip()
    string = "ABCDCDC"
    sub_string = "CDC"
    print("hello hacker")
    count = count_substring(string, sub_string)
    print(count)
